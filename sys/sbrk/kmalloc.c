// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#include <ryukos.h>
#include <sys/sbrk/sbrk.h>
#include <sys/thread/thread.h>
#include <sys/kmutex/kmutex.h>

#include "stdbool.h"
#include "assert.h"

// heavily modified version of K&R malloc.

typedef union header {
    struct {
        union header *ptr;
        usize size;
    } s;
    long _align;
} __attribute__((aligned(4))) Header;

static Header base; /* empty list to get started */
static Header *freep = NULL; /* start of free list */

//TODO clean up spaghetti
void *kmalloc(usize nbytes)
{
    void *out;
    
    KMUTEX_BEGIN();
    
    Header *p, *prevp;
    usize nunits;
    void *cp;

    nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;

    if ((prevp = freep) == NULL) {
        base.s.ptr = freep = prevp = &base;
        base.s.size = 0;
    }

    Header *non_perfect_p = NULL;
    Header *non_perfect_prevp = NULL;

    char non_perfect_found = 0;
    char buddy_found = 0;
    char bud4_found = 0;

    for (p = prevp->s.ptr; ; prevp = p, p = p->s.ptr) {
        /* PERFECT FIT */
        if (p->s.size == nunits) {
            prevp->s.ptr = p->s.ptr;
            freep = prevp;
            out = (void *)(p + 1);
            break;

            /* BUDDY FIT */

        } else if ((p->s.size > nunits) && ((p->s.size & (~0b1)) == ((nunits << 1) & (~0b1)))) {
            /* Found a perfect buddy fit, unless we find a true perfect fit elsewhere,
             * this is probably our best option. */
            non_perfect_p = p;
            non_perfect_prevp = prevp;
            non_perfect_found = 1;
            buddy_found = 1;

            /* BUD4 FIT */

        } else if (((p->s.size > nunits) && ((p->s.size & (~0b11)) == ((nunits << 1) & (~0b11)))) && (buddy_found == 0)) {
            /* This is pretty close to being a proper buddy fit, so it's not as good
             * as a true buddy fit, but it's better than closest fit. Testing showed
             * the 4-block threshold used for bud4 fit is the biggest threshold before
             * it actually starts causing more fragmentation than it fixes. */
            non_perfect_p = p;
            non_perfect_prevp = prevp;
            non_perfect_found = 1;
            bud4_found = 1;

            /* CLOSEST FIT */

        } else if ((p->s.size > nunits) && (non_perfect_found == 1) && (buddy_found == 0) && (bud4_found == 0)) {
            /* found another fit that isn't a buddy or near-buddy fit... is it a closer
             * fit than the last one we found? */
            assert(non_perfect_p != NULL, "illegal state in closestfit in malloc");

            if (p->s.size < non_perfect_p->s.size) {
                non_perfect_p = p;
                non_perfect_prevp = prevp;
                non_perfect_found = 1;
            }

            /* FIRST FIT */

        } else if ((p->s.size > nunits) && (non_perfect_found == 0) && (buddy_found == 0) && (bud4_found == 0)) {
            /* keep track of the first usable space, in case it's the only one that's suitable */
            non_perfect_p = p;
            non_perfect_prevp = prevp;
            non_perfect_found = 1;
        }

        if (p == freep) {
            // made it to the end of the open slots, did we find one that we
            // considered imperfect-but-usable?
            if (non_perfect_found == 1) {
                // sub-optimal spot found, just use it
                p = non_perfect_p;
                prevp = non_perfect_prevp;
                p->s.size -= nunits;
                p += p->s.size;
                p->s.size = nunits;
                freep = prevp;
                out = (void *)(p + 1);
                break;

            } else {
                // no suitable spot found, try to get more space
                cp = sbrk(nunits * sizeof(Header));

                if (cp == (void *) -1) {
                    out = NULL;
                    break;

                } else {
                    p = (Header *) cp;
                    p->s.size = nunits;
                    kfree((void *) (p + 1));
                    p = freep;
                }
            }
        }
    }
    
    KMUTEX_END();
    
    return out;
}

void kfree(void *ap)
{
    Header *bp, *p;
    bp = (Header *) ap - 1;

    for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr) {
        if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
            break;
    }

    if (bp + bp->s.size == p->s.ptr) {
        bp->s.size += p->s.ptr->s.size;
        bp->s.ptr = p->s.ptr->s.ptr;

    } else {
        bp->s.ptr = p->s.ptr;
    }

    if (p + p->s.size == bp) {
        p->s.size += bp->s.size;
        p->s.ptr = bp->s.ptr;

    } else {
        p->s.ptr = bp;
    }

    freep = p;
}
