
#include <assert.h>

#include "../socket.h"
#include "../socket_private.h"
#include "afhdlrs.h"

sockd_t sockafhandler(int domain, int type, int protocol)
{
    assert(domain == AF_AF, "AF_AF handler called for wrong domain");
    
    return ((type == 0) && (domain == 0)) ? (sockd_t)getsockinst() : -1;
}
