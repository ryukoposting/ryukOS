// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#ifndef _RYUKOS_SOCKET_H_
#define _RYUKOS_SOCKET_H_

#include <ryukos.h>

#define AF_AF 0
#define AF_LOCAL 1
#define AF_UNIX AF_LOCAL
#define AF_RYUKOS AF_LOCAL
#define AF_INET 4
#define AF_INET6 6

#ifdef __cplusplus
extern "C" {
#endif
    
    typedef int socklen_t;
    typedef usize sa_family_t;
    typedef int sockd_t;
    
    enum socktypes {
        SOCK_STREAM = 0x01,
        SOCK_DGRAM = 0x02,
        SOCK_SEQPACKET = 0x04,
    };
    
    /* Generic type for socket addresses. Used as a generic cast for "real" socket addresses. */
    struct sockaddr {
        sa_family_t sa_family;
        usize __attribute__((aligned(sizeof(usize)))) _sa_data[4];
    };
    
    typedef struct sockaddr_af {
        sa_family_t saf_family;
    } sockaddr_af_t;
    
    typedef struct sockaddr_loc {
        sa_family_t sun_family;
    } sockaddr_loc_t;
    
    struct msghdr {
        void *msg_name;
        socklen_t *msg_iov;
        int msg_iovlen;
        void *control;
        socklen_t msg_controllen;
        int msg_flags;
    };
    
    void socket_systeminit();
    
    sockd_t socket(int domain, int type, int protocol);
    
    int bind(sockd_t socket, struct sockaddr const *address, socklen_t address_len);
    
    int connect(sockd_t socket, struct sockaddr const *address, socklen_t address_len);
    
    int accept(sockd_t socket, struct sockaddr *restrict address, socklen_t *restrict address_len);
    
    int listen(sockd_t socket, int backlog);
    
    int recv(sockd_t socket, usize *buffer, usize length, int flags);
    
//     int recvfrom(sockd_t socket, usize *restrict buffer, usize length, int flags,
//                struct sockaddr *restrict address, socklen_t *restrict address_len);
    
//     int recvmsg(sockd_t socket, struct msghdr const *message, int flags);
    
    int send(sockd_t socket, usize const *buffer, usize length, int flags);
    
//     int sendto(sockd_t socket, usize const *buffer, usize len, int flags, 
//                struct sockaddr const *dest_addr, socklen_t addrlen);
    
//     int sendmsg(sockd_t socket, struct msghdr const *msg, int flags);

#ifdef __cplusplus
}
#endif

#endif  //_RYUKOS_SOCKET_H_
