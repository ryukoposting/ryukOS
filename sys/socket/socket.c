// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#include <stddef.h>
#include <errno.h>
#include <sys/sbrk/sbrk.h>
#include <sys/kmutex/kmutex.h>
#include "socket.h"
#include "afhdlrs/afhdlrs.h"

#include "socket_private.h"

#define RYUKOS_SOCKDOMCNT 32
#define RYUKOS_SOCKSTARTUPINSTCNT 32

typedef struct {
    sockd_t (*sockhdlr)(int,int,int);
    int (*sockbindhdlr)(sockd_t, struct sockaddr const*, socklen_t);
} aftable_t;

aftable_t aftable[RYUKOS_SOCKDOMCNT] = {
    [AF_AF] = { sockafafhandler }
};

sockinst_t instable[RYUKOS_SOCKSTARTUPINSTCNT];
static sockinst_t *head;


sockinst_t *getsockinst()
{
    sockinst_t *out;
    
    KMUTEX_BEGIN();
    
    if (head != NULL) {
        out = head;
        head = head->next;
        
    } else {
        out = kmalloc(sizeof(sockinst_t) * 4);
        
        head = &out[1];
        head[0].next = &head[1];
        head[1].next = &head[2];
        head[2].next = NULL;
    }
    
    KMUTEX_END();
    
    return out;
}


void socket_systeminit()
{
    for (int i = 1; i < RYUKOS_SOCKSTARTUPINSTCNT; ++i) {
        instable[i - 1].next = &(instable[i]);
    }
    
    instable[RYUKOS_SOCKSTARTUPINSTCNT - 1].next = NULL;
    head = instable;
}

sockd_t socket(int domain, int type, int protocol)
{
    if ((domain >= RYUKOS_SOCKDOMCNT) || (domain < 0)) {
        errno = EINVAL;
        return -1;
    }
    
    if (aftable[domain].sockhdlr == NULL) {
        errno = EAFNOSUPPORT;
        return -1;
    }
    
    return aftable[domain].sockhdlr(domain, type, protocol);
}

int bind(sockd_t socket, struct sockaddr const *address, socklen_t address_len)
{
    sockaddr_af_t const *addr = (sockaddr_af_t const *)address;
    
    return -1;
}


