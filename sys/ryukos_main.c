// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#include <ryukos.h>
#include "ryukos_system.h"
#include <components/ryukos_compinit.h>
#include <semaphore.h>
#include "thread/thread.h"

static sem_t s;

void test2()
{
    yield(-1);
    
    while (1) {
        sem_wait(&s);
    }
}

void test()
{
    sem_init(&s, 0, 1);
    
    while (1) {
        
    }
    
}

usize **activesp;
usize _transitionverify;

void ryukos_main(void)
{
    thread_spawn(test, RYUKOS_DEFAULT_THREADSTACK, 20, THREADPROP_HARD | THREADPROP_DAEMON);
    
    while(1) {
        i16 sched = thread_getnext();
        
        if (sched >= 0) {
            _transitionverify = 0;
            activethreadid = sched;
            activesp = &(threadtable[sched].sp);
            errno = threadtable[sched].errno;
            
            ctswie();
            
            // if we break back to here without setting transitionverify,
            // the user didn't put an exit or restart at the end of their
            // thread
            while (_transitionverify == 0) exit(1);
            
            i16 ind = 0;
            switch (threadtable[activethreadid].state) {
            case THREADSTATE_BLOCKED:
            case THREADSTATE_YIELDED:
                ind = thread_indication();
                if ((ind > 0) && (ind < RYUKOS_MAX_THREADS)) {
                    thread_remschedat(ind, 
                        threadtable[activethreadid].interval > threadtable[ind].interval 
                        ? threadtable[ind].interval : threadtable[activethreadid].interval);
                }
                
                thread_resched(activethreadid);
                break;
                
            case THREADSTATE_SLEEPING:
                thread_sleep(activethreadid);
                break;
                
            case THREADSTATE_DEAD:
                thread_delete(activethreadid);
                break;
                
            case THREADSTATE_EXITED:
                if (threadtable[activethreadid].properties & THREADPROP_DAEMON)
                    thread_restart(activethreadid);
                else
                    thread_delete(activethreadid);
                break;
                
            case THREADSTATE_INVALID:
            case THREADSTATE_UNUSED:
            case THREADSTATE_READY:
                assert(0, "state error");
                while(1);
            }
        }
        
        activethreadid = -1;
    }
    
    while (1);
}


void reset_handler(void)
{
    u32 *idata_begin = &_ldata;
    u32 *data_begin = &_sdata;
    u32 *data_end = &_edata;

    while (data_begin < data_end) *data_begin++ = *idata_begin++;

    u32 *bss_begin = &_sbss;
    u32 *bss_end = &_ebss;

    while (bss_begin < bss_end) *bss_begin++ = 0;
    
    thread_systeminit();
    
#ifdef RYUKOS_PLATFORM_ARM
    interrupts_disable();
    core_configureexceptions();
    core_starttick();
#else
#error "no system tick config defined for this platform, or no platform specified"
#endif  //RYUKOS_PLATFORM_ARM
    
    components_init();
    
    ryukos_main();
}
