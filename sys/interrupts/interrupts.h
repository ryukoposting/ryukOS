// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#ifndef _RYUKOS_INTERRUPTS_H_
#define _RYUKOS_INTERRUPTS_H_

#include <ryukos.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif
    extern int interrupts_irq_disable_nest;

    // These 5 interrupt handlers are required to be defined elsewhere in code.

    // the "system tick," a timed interrupt that switches to kernel context
    void systick_handler(void);

    // svc/service call handler, which does the same thing as systick but is manually called
    void svc_handler(void);

    // reset handler. Runs at startup and any time the device is reset.
    void reset_handler(void);

    // default handler. Any platform-specific fault that is not given a specific
    // implementation should be aliased to the default handler.
    void default_handler(void);
    
    void __attribute__((naked)) pendsv_handler(void);
    
    void entercritical(void);

    void leavecritical(void);
    
    #ifdef RYUKOS_PLATFORM_ARM
    static inline void interrupts_disable(void)
    {
        asm volatile ("cpsid i");
    }
    
    static inline void interrupts_enable(void)
    {
        asm volatile ("cpsie i");
    }
    #else
    #error "Undefined platform"
    #endif //platform defs

#ifdef __cplusplus
}
#endif

#endif
