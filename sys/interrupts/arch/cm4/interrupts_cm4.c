// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#include "../../interrupts.h"
#include "../../../thread/thread.h"
#include <ryukos.h>

#define ICSR_REG (*((u32 *)0xE000ED04))

int interrupts_irq_disable_nest = 0;
extern u8 preempted;
extern u32 **activesp;
// static u32 *nextsp;

void pendsv_handler()
{
}

// void svc_handler()
// {
//     if (interruptlock) return;
//     interruptlock = 1;
//     
//     if (activethreadid >= 0) {
//         
//     }
//     interruptlock = 0;
// }
