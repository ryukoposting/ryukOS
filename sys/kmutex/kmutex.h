// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.



#ifndef _RYUKOS_KMUTEX_H_
#define _RYUKOS_KMUTEX_H_

#include <ryukos.h>
#include <sys/thread/thread.h>

#ifdef __cplusplus
extern "C" {
#endif

#define KMUTEX_BEGIN() static int __kmu\u00F8tex_ = 0; static i16 __kmu\u00F8textid_ = -1; if (__kmu\u00F8tex_ == 0) { __kmu\u00F8tex_ = 1; __kmu\u00F8textid_ = threadid()
    
#define KMUTEX_END() __kmu\u00F8tex_ = 0; __kmu\u00F8textid_ = -1; } else if (threadid() >= 0) yield(__kmu\u00F8textid_)

#ifdef __cplusplus
}
#endif

#endif
