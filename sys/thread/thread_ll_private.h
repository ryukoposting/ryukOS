// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#define LL_END RYUKOS_MAX_THREADS + 1

#ifdef LL_IMPL

threaddesc_t threadtable[RYUKOS_MAX_THREADS];

i16 firstavailable = 0;
i16 lsthead = LL_END;
i16 lsttail = LL_END;
i32 lstmiddleish = LL_END;
u32 lsttail_ticks = LL_END;
u32 lstmiddleish_tickbc = 0x0FFFFFFF; //0FFFFFFF is an arbitrary value- fine
u32 lstmiddleish_ticks = 0x0FFFFFFF;   //as long as it's big

#else

extern i16 lstinsertnew(u32 ticks);
extern i16 lstinsert(i16 id, u32 ticks);
extern int lstremove(i16 id);
extern void lstaddunused(i16 id);
extern i16 lstpop();

// extern threaddesc_t threadtable[RYUKOS_MAX_THREADS];

extern i16 firstavailable;
extern i16 lsthead;
extern i16 lsttail;
extern i16 lstmiddleish;
extern u32 lsttail_ticks;
extern u32 lstmiddleish_tickbc; //0FFFFFFF is an arbitrary value- fine
extern u32 lstmiddleish_ticks;   //as long as it's bigger than 24-bit unsigned max
extern u32 lstaccum;

#endif //LL_IMPL
