// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#include "thread.h"

#define LL_IMPL
#include "thread_ll_private.h"

static void resetmiddleish()
{
    lstmiddleish_tickbc = 0x0FFFFFFF; //0FFFFFFF is an arbitrary value- fine
    lstmiddleish_ticks = 0x0FFFFFFF;   //as long as it's big
    lstmiddleish = LL_END;
}

i16 lstinsertnew(u32 ticks)
{
    if (lsthead == LL_END) {
        lsthead = firstavailable;
        lsttail = firstavailable;
        lsttail_ticks = ticks;
        firstavailable = threadtable[firstavailable].next;
        
        threadtable[lsthead].next = LL_END;
        threadtable[lsthead].tick = ticks;
        return lsthead;
        
    } else if (firstavailable != LL_END) {
        int newidx = firstavailable;
        int i = lsthead;
        int previ = LL_END;
        u32 ticks_before_current_i = 0;
        firstavailable = threadtable[firstavailable].next;
        
        if (ticks <= lsttail_ticks) {
            if (ticks > lstmiddleish_ticks) {
                i = lstmiddleish;
                ticks_before_current_i = lstmiddleish_tickbc;
            }
            
            for (; i != LL_END; i = threadtable[i].next) {
                if (ticks_before_current_i + threadtable[i].tick >= ticks) {
                    if (previ != LL_END) {
                        // inserting into middle of list somewhere
                        threadtable[newidx].next = i;
                        threadtable[previ].next = newidx;
                        
                        // "if ticks is roughly half of lstaccum"
                        // highly imprecise for small average tick counts, but
                        // extremely effective for high average tick counts
                        if ((lsttail_ticks >> 6) == (ticks >> 5)) {
                            lstmiddleish = newidx;
                            lstmiddleish_tickbc = ticks_before_current_i;
                            lstmiddleish_ticks = ticks;
                        }
                        
                    } else {
                        // new list head
                        threadtable[newidx].next = lsthead;
                        lsthead = newidx;
                    }
                    
                    threadtable[newidx].tick = ticks - ticks_before_current_i;
                    threadtable[i].tick -= threadtable[newidx].tick;
                    
                    return newidx;
                }
                
                ticks_before_current_i += threadtable[i].tick;
                previ = i;
            }
            
        } else {
            // new list tail
            threadtable[lsttail].next = newidx;
            threadtable[newidx].tick = ticks - lsttail_ticks;
            threadtable[newidx].next = LL_END;
            lsttail = newidx;
            lsttail_ticks = ticks;
        }
        
        return newidx;
        
    } else return -1;
}


i16 lstinsert(u16 id, u32 ticks) {
    if (lsthead == LL_END) {
        lsthead = id;
        lsttail = id;
        lsttail_ticks = ticks;
        
        threadtable[id].next = LL_END;
        threadtable[id].tick = ticks;
        return id;
        
    } else if (id != LL_END) {
        int i = lsthead;
        int previ = LL_END;
        u32 ticks_before_current_i = 0;
        
        if (ticks <= lsttail_ticks) {
            if (ticks > lstmiddleish_ticks) {
                i = lstmiddleish;
                ticks_before_current_i = lstmiddleish_tickbc;
            }
            
            for (; i != LL_END; i = threadtable[i].next) {
                if (ticks_before_current_i + threadtable[i].tick >= ticks) {
                    if (previ != LL_END) {
                        // inserting into middle of list somewhere
                        threadtable[id].next = i;
                        threadtable[previ].next = id;
                        
                        // "if ticks is roughly half of lstaccum"
                        // highly imprecise for small average tick counts, but
                        // extremely effective for high average tick counts
                        if ((lsttail_ticks >> 6) == (ticks >> 5)) {
                            lstmiddleish = id;
                            lstmiddleish_tickbc = ticks_before_current_i;
                            lstmiddleish_ticks = ticks;
                        }
                        
                    } else {
                        // new list head
                        threadtable[id].next = lsthead;
                        lsthead = id;
                    }
                    
                    threadtable[id].tick = ticks - ticks_before_current_i;
                    threadtable[i].tick -= threadtable[id].tick;
                    
                    return id;
                }
                
                ticks_before_current_i += threadtable[i].tick;
                previ = i;
            }
            
        } else {
            // new list tail
            threadtable[lsttail].next = id;
            threadtable[id].tick = ticks - lsttail_ticks;
            threadtable[id].next = LL_END;
            lsttail = id;
            lsttail_ticks = ticks;
        }
        
        return id;
        
    } else return -1;
}

// TODO: formal verification of this
int lstremove(i16 id)
{
    if (lsthead == LL_END) return -1; //already empty
    if ((id < 0) || (id >= LL_END)) return -2;
    
    register threaddesc_t *self = &(threadtable[id]);
    register i16 next = self->next;
    
    if ((lsthead == lsttail) && (id == lsthead)) { //only element
        lsthead = LL_END;
        lsttail = LL_END;
        lsttail_ticks = 0;
        resetmiddleish();
        
        return 0;
        
    } else if (lsthead == id) { //first element
        threadtable[next].tick += self->tick;
        lsthead = next;
        
        if (lstmiddleish == id) resetmiddleish();
        
        return 0;
            
    } else for (i16 i = lsthead; i != lsttail; ++i) { //arbitrary or last element
        if (threadtable[i].next == id) { // i is the element preceding us
            // check if we are the tail, cleanse tail state variables
            if (next != LL_END) {
                threadtable[next].tick += self->tick;
                
            } else {
                assert(lsttail == id, "next is LL_END but i am not end of list!");
                lsttail = i;
                lsttail_ticks -= self->tick;
            }
            
            // check if we are the middleish, cleanse middleish variables
            if (lstmiddleish == id) resetmiddleish();
            
            threadtable[i].next = self->next;
            
            return 0;
        }
    }
    
    return -1;
}

void lstaddunused(i16 id)
{
    threadtable[id].next = firstavailable;
    firstavailable = id;
}

i16 lstpop()
{
    if ((lsthead == LL_END) || (lsthead < 0)) return -1;
    
    i16 out = lsthead;
    lsthead = threadtable[lsthead].next;
    threadtable[lsthead].tick += threadtable[out].tick;
    
    if (lsttail == out) lsttail = LL_END;
    
    if (lstmiddleish == out) resetmiddleish();
    
    return out;
}
