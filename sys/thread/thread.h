// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#ifndef _RYUKOS_THREAD_H_
#define _RYUKOS_THREAD_H_

#include <ryukos.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

#if (RYUKOS_MAX_THREADS > 1024)
#error "RYUKOS_MAX_THREADS is too large (max: 1024)"
#endif

#define THREAD_NOAVAILABLE -1
#define THREAD_BIGSTACK -2
#define THREAD_NOMEM -3
#define THREAD_INVALIDPRIO -4
#define THREAD_TOOMANYRT -5

#ifdef __cplusplus
extern "C" {
#endif
    
    typedef enum {
        THREADSTATE_UNUSED = 0,     // do not change, keep zero.
        THREADSTATE_BLOCKED = 1,   // preempted or daemon restart
        THREADSTATE_INVALID = 2,
        THREADSTATE_SLEEPING = 3,
        THREADSTATE_READY = 4,
        THREADSTATE_YIELDED = 5,
        THREADSTATE_DEAD = 6,
        THREADSTATE_EXITED = 7
    } threadstate_t;
    
    typedef enum {
        THREADPROP_HARD = 0x8000,
        THREADPROP_DAEMON = 0x4000,
        THREADPROP_SLEEPYDAEMON = 0x6000,
    } threadprops_t;
    
    typedef struct {
        // don't change the order of these
        usize *sp;
        usize *mem;
        usize state;
        usize stacksize;
        usize errno;
        void (*fn)(void);
        
        // LL
        i16 next;
        i16 tick;
        
        // PROPS
        u16 interval;
        u16 properties;
        
        i16 indication;
        i16 currindlevel;
    } __attribute__((packed)) __attribute__((aligned(4)))  threaddesc_t;
    
    extern threaddesc_t threadtable[RYUKOS_MAX_THREADS];
    extern usize *main_stk;
    extern i16 activethreadid;
    extern void ctswie();
    extern void ctswid();
    
    /**
     * @brief Initialize the variables and such used by the
     * multithreading system.
     */
    void thread_systeminit();
    
    /**
     * @brief Used to restart daemons when they exit.
     */
    void thread_restart(i16 id);

    /**
     * @brief Create a new thread and schedule it.
     */
    i16 thread_spawn(void (*fn)(void), usize stacksize, i16 interval, u16 properties);
    
    /**
     * @brief Create a new thread, leaving it in sleeping state.
     */
    i16 thread_create(void (*fn)(void), usize stacksize, i16 interval, u16 properties);
    
    /**
     * @brief Delete a thread. Should not be called on the currently-active
     * thread.
     */
    void thread_delete(i16 id);
    
    /**
     * @brief Reschedule the thread, putting it into ready state.
     */
    void thread_resched(i16 id);
    
    /**
     * @brief Reschedule a currently-scheduled thread at a specific interval, putting it into ready state.
     */
    void thread_remschedat(i16 id, u16 interval);
    
    /**
     * @brief Reschedule the thread, putting it to sleep.
     */
    void thread_sleep(i16 id);
    
    /**
     * @brief Put a thread into ready state. Does not reschedule the thread,
     */
    void thread_awaken(i16 id);
    
    /**
     * @brief Returns true if we are currently in a userspace thread.
     */
    bool thread_inuserspace();
    
    /**
     * @brief Check whether a thread gave a valid indicator.
     */
    i16 thread_indication();
    
    /**
     * @return The ID of the active user thread, or -1 if in the scheduler.
     */
    i16 thread_activeid();
    
    /**
     * @return If no events have given us a "best" thread to run next, use
     * this to grab the next-highest-priority thread that can be run
     */
    i16 thread_getnext();
    
    i16 threadid();
    
    i16 fork(); //NYI
    
    void yield(i16 indicate);
    
    void exit(int retval);
    
    int isalive(i16 tid);
    
    /* exit(1) synonyms, provided for compatibility */
    void _exit(int retval);
    void __exit(int retval);
    void ___exit(int retval);

#ifdef __cplusplus
}
#endif

#endif  //_RYUKOS_THREAD_H_
