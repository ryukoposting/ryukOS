// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.


/*NOTE: if I ever go up to something like a Cortex-A or really anything
  with cache, the entire thread backend needs to be re-written. This is the
  literal definition of un-cache-friendly code. */

#include "thread.h"
#include <stdbool.h>
#include <assert.h>
#include <sys/interrupts/interrupts.h>
#include "../sbrk/sbrk.h"

#include "thread_ll_private.h"

i16 activethreadid = -2;
usize *main_stk;

void thread_systeminit()
{
    for (usize i = 0; i < RYUKOS_MAX_THREADS; ++i) {
        threadtable[i].next = i + 1;
        threadtable[i].tick = 0;
        threadtable[i].state = THREADSTATE_UNUSED;
        threadtable[i].indication = -1;
        threadtable[i].currindlevel = -1;
    }
    
    firstavailable = 0;
    lsthead = LL_END;
    lsttail = LL_END;
    lstmiddleish = LL_END;
    lstmiddleish_tickbc = 0x0FFFFFFF;
    lstmiddleish_ticks = 0x0FFFFFFF;
}

void thread_restart(i16 id)
{
    register threaddesc_t *newt = &(threadtable[id]);
    
    newt->state = THREADSTATE_INVALID;
    
#ifdef RYUKOS_PLATFORM_ARM
    newt->mem[newt->stacksize - 1] = (usize)newt->fn;
    newt->sp = &(newt->mem[newt->stacksize - 14]);
#else
#error
#endif
    if ((newt->properties & THREADPROP_SLEEPYDAEMON) == THREADPROP_SLEEPYDAEMON)
        newt->state = THREADSTATE_SLEEPING;
    else
        newt->state = THREADSTATE_BLOCKED;
    
    lstinsert(id, newt->interval);
}

i16 thread_create(void (*fn)(void), usize stacksize, i16 interval, u16 properties)
{
    BEGIN_CRITICAL;
    
    i16 newtid = lstinsertnew(interval);
    
    if (newtid == -1)
        RETURN_CRITICAL(THREAD_NOAVAILABLE);
    
    if (interval < 0)
        RETURN_CRITICAL(THREAD_INVALIDPRIO);
    
    register threaddesc_t *newt = &(threadtable[newtid]);
    
    newt->state = THREADSTATE_INVALID;
    
    if (stacksize > RYUKOS_MAX_THREADSTACK)
        RETURN_CRITICAL(THREAD_BIGSTACK);

    
    // normalize stack size
    stacksize = (stacksize < RYUKOS_MIN_THREADSTACK) ? RYUKOS_MIN_THREADSTACK : stacksize;
    usize memincr = (stacksize % RYUKOS_INCR_THREADSTACK);
    if (memincr != 0) {
        stacksize -= memincr;
        stacksize += RYUKOS_INCR_THREADSTACK;
    }
    
    // try to allocate stack space
    usize *mem = (usize *)kmalloc(stacksize);
    if (mem == NULL)
        RETURN_CRITICAL(THREAD_NOMEM);
    
#ifdef RYUKOS_PLATFORM_ARM
    /*
     stacksize - 1: fn
     */
    newt->mem = mem;
    newt->mem[stacksize - 1] = (usize)fn;
    newt->fn = fn;
    newt->sp = &(newt->mem[stacksize - 14]);
    newt->interval = interval;
    newt->properties = properties;
    newt->stacksize = stacksize;
    newt->state = THREADSTATE_SLEEPING;
#else
#error
#endif
    
    RETURN_CRITICAL(newtid);
}

i16 thread_spawn(void (*fn)(void), usize stacksize, i16 interval, u16 properties)
{
    BEGIN_CRITICAL;
    
    register i16 newtid = thread_create(fn, stacksize, interval, properties);
    
    if ((newtid >= RYUKOS_MAX_THREADS) || (newtid < 0)) RETURN_CRITICAL(newtid);
    
    threadtable[newtid].state = THREADSTATE_READY;
    
    RETURN_CRITICAL(newtid);
}

void thread_delete(i16 id)
{
    BEGIN_CRITICAL;
    
    if ((id < 0) || (id >= RYUKOS_MAX_THREADS)) RETURN_CRITICAL();
    if ((threadtable[id].state == THREADSTATE_UNUSED) || (threadtable[id].state == THREADSTATE_INVALID)) RETURN_CRITICAL();
    
    register threaddesc_t *it = &(threadtable[id]);
    kfree(it->mem);
    lstaddunused(id);
    
    it->state = THREADSTATE_UNUSED;
    
    RETURN_CRITICAL();
}

void thread_resched(i16 id)
{
    BEGIN_CRITICAL;
    
    register threaddesc_t *it = &(threadtable[id]);
    lstinsert(id, it->interval);
    
    it->state = THREADSTATE_BLOCKED;
    
    RETURN_CRITICAL();
}

void thread_remschedat(i16 id, u16 interval)
{
    BEGIN_CRITICAL;
    
    if ((id < 0) || (id >= RYUKOS_MAX_THREADS)) RETURN_CRITICAL();
    if ((threadtable[id].state == THREADSTATE_UNUSED) || (threadtable[id].state == THREADSTATE_INVALID)) RETURN_CRITICAL();
    if ((threadtable[id].currindlevel != -1)) RETURN_CRITICAL();
    
    assert(lstremove(id) == 0, "failure in lstremove");
    threadtable[id].currindlevel = interval;
    lstinsert(id, interval);
    
    threadtable[id].state = THREADSTATE_BLOCKED;
    
    RETURN_CRITICAL();
}

void thread_sleep(i16 id)
{
    BEGIN_CRITICAL;
    
    if ((id < 0) || (id >= RYUKOS_MAX_THREADS)) RETURN_CRITICAL();
    if ((threadtable[id].state == THREADSTATE_UNUSED) || (threadtable[id].state == THREADSTATE_INVALID)) RETURN_CRITICAL();
    
    register threaddesc_t *it = &(threadtable[id]);
    lstinsert(id, it->interval);
    
    it->state = THREADSTATE_SLEEPING;
    
    RETURN_CRITICAL();
}

i16 thread_getnext()
{
    BEGIN_CRITICAL;
    
    register i16 tid = lstpop();
    
    if ((tid < 0) || (tid >= RYUKOS_MAX_THREADS)) RETURN_CRITICAL(-1);
    
    threadtable[tid].indication = -1;
    threadtable[tid].currindlevel = -1;
    
    RETURN_CRITICAL(tid);
}

i16 thread_indication()
{
    return threadtable[activethreadid].indication;
}

i16 threadid()
{
    return activethreadid;
}

i16 fork()
{
    //TODO
    yield(-1);
    return -1;
}

void _exit(int retval) __attribute((weak, alias("exit")));
void __exit(int retval) __attribute((weak, alias("exit")));
void ___exit(int retval) __attribute((weak, alias("exit")));

void yield(i16 indicate)
{
    interrupts_disable();
    threadtable[activethreadid].indication = indicate;
    threadtable[activethreadid].errno = errno;
    threadtable[activethreadid].state = THREADSTATE_YIELDED;
    ctswid();
}

int isalive(i16 tid)
{
    if ((tid < 0) || (tid >= RYUKOS_MAX_THREADS)) return -1;
    
    switch (threadtable[tid].state) {
        case THREADSTATE_BLOCKED:
        case THREADSTATE_READY:
        case THREADSTATE_YIELDED:
        case THREADSTATE_SLEEPING:
            return 1;
            
        default:
            return 0;
    }
}

void exit(int retval)
{   
    UNUSED(retval);     //TODO: use retval for... something
    interrupts_disable();
    threadtable[activethreadid].errno = errno;
    threadtable[activethreadid].state = THREADSTATE_EXITED;
    ctswid();
    assert(0,"exit failed");
}
