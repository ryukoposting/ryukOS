// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#ifndef _RYUKOS_TIMER_H_
#define _RYUKOS_TIMER_H_

#include <ryukos.h>

#if (RYUKOS_MAX_TIMERS > 255)
#error "Max value for RYUKOS_MAX_TIMERS is 255"
#endif

#ifdef __cplusplus
extern "C" {
#endif
    
    typedef i32 tmr_t;
    
    void timer_systeminit();
    
    tmr_t timer_begin(u32 ticks);
    
    u32 timer_read(tmr_t tid);
    
    int timer_tick();
    
    void timer_isr();

#ifdef __cplusplus
}
#endif

#endif  //_RYUKOS_TIMER_H_
