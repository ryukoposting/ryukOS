// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#include <ryukos.h>
#include "../../timer.h"

#ifdef RYUKOS_PLATFORMSPEC_XMC4700

#define slice_ptr         CCU42_CC43
#define module_ptr            CCU42
#define module_num     (2U)
#define slice_num      (3U)
#define capcom_msk       (SCU_GENERAL_CCUCON_GSC42_Msk) /**< Only CCU42 */

void timer_isr()
{
//     XMC_CCU4_SLICE_ClearEvent(SLICE_PTR, XMC_CCU4_SLICE_IRQ_ID_PERIOD_MATCH);
    timer_tick();
}

void timer_platforminit()
{
    // XMC_CCU4_SetModuleClock
    u32 cntl = module_ptr->GCTRL;
    cntl &= ~((u32) CCU4_GCTRL_PCIS_Msk);
    module_ptr->GCTRL = cntl;
    
    // XMC_CCU4_Init
    cntl &= ~((u32) CCU4_GCTRL_MSDE_Msk);
    module_ptr->GCTRL = cntl;
    
    // XMC_CCU4_EnableClock
    module_ptr->GIDLC |= (1 << slice_num);
    
    // XMC_CCU4_StartPrescaler
    module_ptr->GIDLC |= CCU4_GIDLC_SPRB_Msk;
    
    // XMC_CCU4_SLICE_CompareInit
    slice_ptr->TC = 0;
    slice_ptr->CMC = (0 << CCU4_CC4_CMC_TCE_Pos);
    
    // XMC_CCU4_SLICE_EnableEvent
    //enable a period match event
    //compare match counting up would be 1 << 2
    //compare match  counting down would be 1 << 3
    slice_ptr->INTE = (1 << 0);
    
    // XMC_CCU4_SLICE_SetInterruptNode
    //period match event- attach to SCU SR0
    //this stuff will be different depending on
    //whether we do a period match, compare match, etc
    u32 srs = slice_ptr->SRS;
    u32 mask = CCU4_CC4_SRS_POSR_Msk;
    u32 pos = CCU4_CC4_SRS_POSR_Pos;
    
    srs &= ~mask;
    // 0 = SR0, 1 = SR1, etc.
    srs |= (0 << pos);
    
    slice_ptr->SRS = srs;
    
    //NVIC_SetPriority(CCU42_0_IRQn, prio);
    //NVIC_EnableIRQ(CCU42_0_IRQn);
    
    //XMC_CCU4_SLICE_SetTimerPeriodMatch(SLICE_PTR, some_number_here);
    // if we switch to compare: XMC_CCU4_SLICE_SetTimerCompareMatch(SLICE_PTR, 32000U);
    
    
    
}

#endif
