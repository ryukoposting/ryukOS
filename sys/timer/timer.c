// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#include "timer.h"
#include "../thread/thread.h"

#define TIMERLS_END RYUKOS_MAX_TIMERS

extern void timer_platforminit();

static struct {
    i16 threadid;
    u32 next    : 8;
    u32 tick    : 24;
} tmrls[RYUKOS_MAX_TIMERS];

static int firstopen = 0;
static int firstinuse = TIMERLS_END;
static int lastinuse = TIMERLS_END;
static int recentinuse = TIMERLS_END;
static u32 lastinuse_ticks = TIMERLS_END;
static u32 recentinuse_tickbc = 0x0FFFFFFF; //0FFFFFFF is an arbitrary value- fine
static u32 recentinuse_ticks = 0x0FFFFFFF;   //as long as it's bigger than 24-bit unsigned max
static u32 currtop = 0;

void timer_systeminit()
{
    for (usize i = 0; i < RYUKOS_MAX_TIMERS; ++i) {
        tmrls[i].next = i + 1;
        tmrls[i].tick = 0;
        tmrls[i].threadid = -1;
    }
    
    firstopen = 0;
    firstinuse = TIMERLS_END;
    lastinuse = TIMERLS_END;
    recentinuse = TIMERLS_END;
    recentinuse_tickbc = 0x0FFFFFFF;
    recentinuse_ticks = 0x0FFFFFFF;
    
    timer_platforminit();
}

int timer_tick()
{
    if (firstinuse != TIMERLS_END) {
        recentinuse_ticks -= 1;
        currtop -= 1;
        tmrls[firstinuse].tick -= 1;
    }
    return tmrls[firstinuse].tick;
}

tmr_t timer_begin(u32 ticks)
{
    if (firstinuse == TIMERLS_END) {
        firstinuse = firstopen;
        lastinuse = firstopen;
        lastinuse_ticks = ticks;
        currtop = ticks;
        firstopen = tmrls[firstopen].next;
        
        tmrls[firstinuse].next = TIMERLS_END;
        tmrls[firstinuse].tick = ticks;
        tmrls[firstinuse].threadid = threadid();
        return firstinuse;
        
    } else if (firstopen != TIMERLS_END) {
        int newidx = firstopen;
        int i = firstinuse;
        int previ = TIMERLS_END;
        u32 ticks_before_current_i = 0;
        firstopen = tmrls[firstopen].next;
        
        if (ticks <= lastinuse_ticks) {
            if (ticks > recentinuse_ticks) {
                i = recentinuse;
                ticks_before_current_i = recentinuse_tickbc;
            }
            
            for (; i != TIMERLS_END; i = tmrls[i].next) {
                if (ticks_before_current_i + tmrls[i].tick >= ticks) {
                    if (previ != TIMERLS_END) {
                        // inserting into middle of list somewhere
                        tmrls[newidx].next = i;
                        tmrls[previ].next = newidx;
                        
                        // "if ticks is roughly half of currtop"
                        // highly imprecise for small average tick counts, but
                        // extremely effective for high average tick counts
                        if ((currtop >> 6) == (ticks >> 5)) {
                            recentinuse = newidx;
                            recentinuse_tickbc = ticks_before_current_i;
                            recentinuse_ticks = ticks;
                        }
                        
                    } else {
                        // new list head
                        tmrls[newidx].next = firstinuse;
                        firstinuse = newidx;
                    }
                    
                    tmrls[newidx].tick = ticks - ticks_before_current_i;
                    tmrls[newidx].threadid = threadid();
                    tmrls[i].tick -= tmrls[newidx].tick;
                    
                    return newidx;
                }
                
                ticks_before_current_i += tmrls[i].tick;
                previ = i;
            }
            
        } else {
            // new list tail
            tmrls[lastinuse].next = newidx;
            tmrls[newidx].tick = ticks - lastinuse_ticks;
            tmrls[newidx].next = TIMERLS_END;
            tmrls[newidx].threadid = threadid();
            lastinuse = newidx;
            lastinuse_ticks = ticks;
            currtop = ticks;
        }
        
        return newidx;
        
    } else return -1;
}

u32 timer_read(tmr_t tid)
{
    u32 accum = 0;
    for (int i = firstinuse; i != tid; i = tmrls[i].next) {
        accum += tmrls[i].tick;
    }
    
    return accum + tmrls[tid].tick;
}
