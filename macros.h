
#ifndef _RYUKOS_MACRO_H_
#define _RYUKOS_MACRO_H_

#if defined(__GNUC__) || defined(__clang__)
#define PRAGMA(_x) _Pragma(#_x)
#define restrict __restrict__

#elif defined(_MSC_VER)
#define PRAGMA(_x) __pragma(#_x)
#define restrict __restrict
#define aligned(_n) __declspec(align(_n))
/* No decent way of using MSVC's pack, also no real reason
 * to support MSVC yet, so just throw an error */
// #define packed() __pragma(pack(push, 1))
#error MSVC not supported due to issues with its packing directive.

#else
#error "compiler not supported"
#endif

// #include "sys/interrupts/interrupts.h"

#define MACRO_CONCAT_(_a, _b) _a##_b
#define MACRO_CONCAT(_a, _b) MACRO_CONCAT_(_a, _b)

#define STRINGIFY_(_s) #_s
#define STRINGIFY(_s) STRINGIFY_(_s)

#define UNUSED(_x) ((void)_x)

// used to get linker label names that follow RyukOS convention
#define SECTION_START_(_section) _s##_section
#define SECTION_START(_section) SECTION_START_(_section)
#define SECTION_END_(_section) _e##_section
#define SECTION_END(_section) SECTION_END_(_section)
#define SECTION_SIZE_(_section) _z##_section
#define SECTION_SIZE(_section) SECTION_SIZE_(_section)
#define SECTION_LOAD_(_section) _l##_section
#define SECTION_LOAD(_section) SECTION_LOAD_(_section)

// ONLY USED BY SCHEDULER-RELATED FUNCTIONS. For anything else,
// KMutexes should be used. (currently, the _critical ops are
// unneeded because interrupts are disabled while the scheduler
// is running)
#define BEGIN_CRITICAL 
#define END_CRITICAL 
#define RETURN_CRITICAL(_retval) { return _retval; }

#endif
