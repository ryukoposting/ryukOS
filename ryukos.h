// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#ifndef _RYUKOS_GLOBAL_H_
#define _RYUKOS_GLOBAL_H_

/************** HARDWARE PLATFORM PROPERTIES **************/
#define RYUKOS_PLATFORM_ARM
// #define RYUKOS_PLATFORM_RISCV
#define RYUKOS_PLATFORM_ARM_M4
// #define RYUKOS_PLATFORM_ARM_M3
// #define RYUKOS_SIMULATOR
#define RYUKOS_MAX_HEAP 0x10000

#define RYUKOS_PLATFORM_IPC 144000000

/***************** STANDARD OS PROPERTIES *****************/

/**
 * @brief the maximum number of sockets that can be in use
 * on the machine at the same time
 */
#define RYUKOS_MAX_SOCK_COUNT 256

/**
 * @brief the maximum number of threads that can be
 * registered on the machine at the same time. Should pretty
 * much always be less than RYUKOS_MAX_SOCK_COUNT.
 */
#define RYUKOS_MAX_THREADS 64

/**
 * @brief the default stack space allocated to a thread.
 */
#define RYUKOS_DEFAULT_THREADSTACK 1024

/**
 * @brief the maximum amound of space that can be allocated
 * to a single thread. Asking for more than this amount will
 * return an error and the thread will not be created.
 */
#define RYUKOS_MAX_THREADSTACK 4096

/**
 * @brief the minimum amount of stack space that will be
 * allocated to a single thread. Asking for less than this
 * amount will not return an error and the thread will
 * still be created, but with this much stack space.
 */
#define RYUKOS_MIN_THREADSTACK 512

/**
 * @brief The amount of stack space given to a thread will
 * always be a factor of this number. Should pretty much
 * always be a power of 2, and equal to/less than
 * RYUKOS_MIN_THREADSTACK.
 */
#define RYUKOS_INCR_THREADSTACK 512

/**
 * @brief The max number of realtime threads that may be
 * created.
 */
#define RYUKOS_MAX_RT_THREADS 4

#define RYUKOS_MAX_TIMERS 127

/**
 * @brief If RYUKOS_USE_COOPERATIVE_SCHEDULER is defined,
 * interrupt-based preemption will be disabled. Context
 * switches will only occur when threads explicitly yield.
 */
// #define RYUKOS_USE_COOPERATIVE_SCHEDULER

/**
 * @brief The frequency of the tick rate. That is, the tick
 * rate of the OS is (1/RYUKOS_TICK_FREQ).
 */
#define RYUKOS_TICK_FREQ 100

/**
 * @brief A thread that is rescheduled may be automatically
 * indicated, signaling that it should be run again quickly.
 * This can occur at least once every RYUKOS_MAX_COOLDOWN
 * reschedule-ings. Threads whose priority number is lower
 * than RYUKOS_MAX_COOLDOWN will never wait more than their
 * priority number count of reschedule-ings before automatic
 * indication (in other words, higher priority threads can
 * be automatically indicated at a faster rate) If RyukOS
 * is at idle, it will happen more frequently for all threads.
 * max value = 0x3F
 */
#define RYUKOS_MAX_COOLDOWN 0x0F

/**
 * @brief if
 */
#define RYUKOS_PLATFORM_TICK_GUESS 144000

/*********************** COMPONENTS ***********************/
/**
 * @brief Include the TCP/IP stack component.
 * 
 * Options:
 *   - COMP_TCPIP_OPTION_NOIPV6: exclude IPV6 subcomponent.
 *   - COMP_TCPIP_OPTION_HASPROFINET: include PROFINET subcomponent.
 *   - COMP_TCPIP_OPTION_HASWAKEONLAN: include Wake-On-LAN subcomponent.
 */
#define COMP_TCPIP
// #define COMP_TCPIP_OPTION_NOIPV6
// #define COMP_TCPIP_OPTION_HASPROFINET
// #define COMP_TCPIP_OPTION_HASWAKEONLAN

/**
 * @brief Include the serial communications component.
 * 
 * Options:
 *   - COMP_SERIAL_OPTION_NOI2C: exclude I2C subcomponent.
 *   - COMP_SERIAL_OPTION_NOSPI: exclude SPI subcomponent.
 */
#define COMP_SERIAL
// #define COMP_SERIAL_OPTION_NOI2C
// #define COMP_SERIAL_OPTION_NOSPI
// #define COMP_SERIAL_OPTION_USBINTEROP

/**
 * @brief Include the RyuFS component.
 * 
 * Options:
 *   - COMP_SERIAL_OPTION_NOSDMMC: exclude SDMMC subcomponent.
 *   - COMP_SERIAL_OPTION_NOOBF: exclude On-Board Flash subcomponent.
 */
#define COMP_FS
// #define COMP_SERIAL_OPTION_NOSDMMC
// #define COMP_SERIAL_OPTION_NOOBF

/**
 * @brief Include the USB communications component.
 * 
 * Options:
 *   - COMP_SERIAL_OPTION_NOSDMMC: exclude SDMMC subcomponent.
 *   - COMP_SERIAL_OPTION_NOOBF: exclude On-Board Flash subcomponent.
 *   - COMP_SERIAL_OPTION_USBINTEROP: include interoperability with Serial component.
 */
#define COMP_USB
// #define COMP_USB_OPTION_NOHOST
// #define COMP_USB_OPTION_NODEVICE

#define COMP_TIME
// #define COMP_TIME_OPTION_NORTC

#define COMP_CAN
// #define COMP_CAN_OPTION_HASDEVICENET

#define COMP_GPIO
// #define COMP_GPIO_OPTION_NOPWM
// #define COMP_GPIO_OPTION_NOADC
// #define COMP_GPIO_OPTION_NODAC

/********************** MISCELLANEOUS *********************/
// #define NDEBUG
// #define NDEBUG_CT

#endif      //_RYUKOS_GLOBAL_H_

#include <macros.h>
#include <platform/ryukos_platform.h>
#include <components/ryukos_components.h>
