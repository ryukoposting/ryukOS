// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#ifndef _RYUKOS_PLATFORM_H_
#define _RYUKOS_PLATFORM_H_

#if defined(RYUKOS_PLATFORM_ARM_M3) || defined(RYUKOS_PLATFORM_ARM_M4)

#define u8 unsigned char
#define u16 unsigned short
#define u32 unsigned int
#define u64 unsigned long long

#define i8 signed char
#define i16 signed short
#define i32 signed int
#define i64 signed long long

#define usmall8 u8 __attribute__((packed))
#define usmall16 u16 __attribute__((packed))
#define usmall32 u32 __attribute__((packed))
#define usmall64 u64 __attribute__((packed))

#define ismall8 i8 __attribute__((packed))
#define ismall16 i16 __attribute__((packed))
#define ismall32 i32 __attribute__((packed))
#define ismall64 i64 __attribute__((packed))

#define usize u32

#define f32 float
#define f64 double

#ifndef __INT32_MAX__
#define __INT32_MAX__ 0x7FFFFFFF
#endif

#ifndef INT32_MAX
#define INT32_MAX __INT32_MAX__
#endif

#ifndef __INT32_MIN__
#define __INT32_MIN__ 0x80000000
#endif

#ifndef INT32_MIN
#define INT32_MIN __INT32_MIN__
#endif

#endif //defined(RYUKOS_PLATFORM_ARM_M3) || defined(RYUKOS_PLATFORM_ARM_M4)

#include <core.h>
#include <regdefs.h>
#include <ldlabels.h>

#endif //_RYUKOS_PLATFORM_H_
