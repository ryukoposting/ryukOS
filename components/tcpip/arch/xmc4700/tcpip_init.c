// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#include "../../tcpip_global.h"
#include <xmc_gpio.h>
#include <xmc_eth_mac.h>
#include <xmc_eth_phy.h>
#include <assert.h>


extern macaddr_t tcpip_macaddr;

#define RXD1     P2_3
#define RXD0     P2_2
#define RXER     P2_4
#define CLK_RMII P15_8
#define TX_EN    P2_5
#define TXD1     P2_9
#define TXD0     P2_8
#define CRS_DV   P15_9
#define MDIO     P2_0
#define MDC      P2_7

static __attribute__((aligned(4))) XMC_ETH_MAC_DMA_DESC_t rx_desc[4] __attribute__((section ("ETH_RAM")));
static __attribute__((aligned(4))) XMC_ETH_MAC_DMA_DESC_t tx_desc[8] __attribute__((section ("ETH_RAM")));
static __attribute__((aligned(4))) u8 rx_buf[4][8] __attribute__((section ("ETH_RAM")));
static __attribute__((aligned(4))) u8 tx_buf[8][8] __attribute__((section ("ETH_RAM")));

void tcpip_init()
{
    XMC_ETH_MAC_PORT_CTRL_t port_control;
    XMC_GPIO_CONFIG_t gpio_config;

//     ETHIF_t *ethif = netif->state;
    XMC_ETH_MAC_t mac = {
        .regs = ETH0,
        .rx_desc = rx_desc,
        .tx_desc = tx_desc,
        .rx_buf = &rx_buf[0][0],
        .tx_buf = &tx_buf[0][0],
        .num_rx_buf = 4,
        .num_tx_buf = 8
    };

    XMC_ETH_PHY_CONFIG_t phyconf = {
        .interface = XMC_ETH_LINK_INTERFACE_RMII,
        .enable_auto_negotiate = true,
    };

    XMC_ETH_MAC_Enable(&mac);
    XMC_ETH_MAC_SetManagmentClockDivider(&mac);

    /* Do whatever else is needed to initialize interface. */
    gpio_config.mode = XMC_GPIO_MODE_INPUT_TRISTATE;
    XMC_GPIO_Init(RXD0, &gpio_config);
    XMC_GPIO_Init(RXD1, &gpio_config);
    XMC_GPIO_Init(CLK_RMII, &gpio_config);
    XMC_GPIO_Init(CRS_DV, &gpio_config);
    XMC_GPIO_Init(RXER, &gpio_config);
    XMC_GPIO_Init(MDIO, &gpio_config);

    port_control.mode = XMC_ETH_MAC_PORT_CTRL_MODE_RMII;
    port_control.rxd0 = XMC_ETH_MAC_PORT_CTRL_RXD0_P2_2;
    port_control.rxd1 = XMC_ETH_MAC_PORT_CTRL_RXD1_P2_3;
    port_control.clk_rmii = XMC_ETH_MAC_PORT_CTRL_CLK_RMII_P15_8;
    port_control.crs_dv = XMC_ETH_MAC_PORT_CTRL_CRS_DV_P15_9;
    port_control.rxer = XMC_ETH_MAC_PORT_CTRL_RXER_P2_4;
    port_control.mdio = XMC_ETH_MAC_PORT_CTRL_MDIO_P2_0;
    XMC_ETH_MAC_SetPortControl(&mac, port_control);

    XMC_GPIO_SetHardwareControl(MDIO, XMC_GPIO_HWCTRL_PERIPHERAL1);

    gpio_config.output_level = XMC_GPIO_OUTPUT_LEVEL_LOW;
    gpio_config.output_strength = XMC_GPIO_OUTPUT_STRENGTH_STRONG_MEDIUM_EDGE;
    gpio_config.mode = (XMC_GPIO_MODE_t)((u32)XMC_GPIO_MODE_OUTPUT_PUSH_PULL | P2_7_AF_ETH0_MDC);
    XMC_GPIO_Init(MDC, &gpio_config);

//    assert(XMC_ETH_PHY_Init(&mac, 0, &phyconf) == XMC_ETH_PHY_STATUS_OK, "PHY initialization failed.");
    XMC_ETH_PHY_Init(&mac, 0, &phyconf);

    gpio_config.mode = (XMC_GPIO_MODE_t)((u32)XMC_GPIO_MODE_OUTPUT_PUSH_PULL |P2_8_AF_ETH0_TXD0);
    XMC_GPIO_Init(TXD0, &gpio_config);
    gpio_config.mode = (XMC_GPIO_MODE_t)((u32)XMC_GPIO_MODE_OUTPUT_PUSH_PULL | P2_9_AF_ETH0_TXD1);

    XMC_GPIO_Init(TXD1, &gpio_config);

    gpio_config.mode = (XMC_GPIO_MODE_t)((u32)XMC_GPIO_MODE_OUTPUT_PUSH_PULL | P2_5_AF_ETH0_TX_EN);
    XMC_GPIO_Init(TX_EN, &gpio_config);

    XMC_ETH_MAC_InitEx(&mac);
    XMC_ETH_MAC_DisableJumboFrame(&mac);
//     XMC_ETH_MAC_SetAddressEx(&mac, tcpip_macaddr);
}

