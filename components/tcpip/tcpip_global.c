#include "tcpip_global.h"
#include <string.h>

macaddr_t tcpip_macaddr = {
    .addr = { 0x00, 0x01, 0x13, 0x35, 0x57, 0x79 }
};

void tcpip_setmacaddr(macaddr_t const *const addr)
{
    memncpy(&tcpip_macaddr, addr, 6);
}
