
#ifndef RYUKOLIB_VARIANT_H_
#define RYUKOLIB_VARIANT_H_

#include "misc.h"

#define vmatch(variant_obj,vcases) {auto _vobj_ = &variant_obj; switch(variant_obj.vtype) vcases}
#define vcase(variant_class,variant_member_name,method) case variant_class::variant_member_name: \
{ auto match = &(_vobj_->_data_.MACRO_CONCAT(variant_member_name,_data_));\
    method;\
} break

#define rlib_variant(class_name) struct class_name
#define rlib_variant_types enum _vtypes_ 
#define rlib_variant_data union _dtypes_
#define rlib_variant_init _vtypes_ vtype; _dtypes_ _data_
#define rlib_variant_data_member(type,contents) struct contents MACRO_CONCAT(type,_data_);

#endif //RYUKOLIB_VARIANT_H_
