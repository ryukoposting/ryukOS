#include "hash.h"

namespace rlib {
namespace hash {
    /* The Jenkins one-at-a-time hash function. Brutally simple, pretty
     * effective, not overwhelmingly performant. */
    u32 JenkinsOneAtATime(const u8 *vals, usize len) {
        usize i = 0;
        u32 hash = 0;
        while (i <= len) {
            hash += vals[i++];
            hash += hash << 10;
            hash ^= hash >> 6;
        }
        hash += hash << 3;
        hash ^= hash >> 11;
        hash += hash << 15;
        return hash;
    }
    
    //NOTE: this is only a valid way of doing things internal to the system,
    //since the output is dependent on endianness.
    u32 JenkinsOneAtATime(const u16 *vals, usize len) {
        return JenkinsOneAtATime((const u8*)vals, len * 2);
    }
    
    //NOTE: this is only a valid way of doing things internal to the system,
    //since the output is dependent on endianness.
    u32 JenkinsOneAtATime(const u32 *vals, usize len) {
        return JenkinsOneAtATime((const u8*)vals, len * 4);
    }
    
    /* FNV-1A, 32-bit flavor. Even simpler than Jenkins, and slightly
     * faster, too. Apparently it also strikes a great balance of
     * rare collisions and even distribution. */
    u32 FNV1A_32(const u8 vals[], usize len) {
        static const u32 prime = 16777619;
        static const u32 offset = 0x811c9dc5;
        
        u32 hash = offset;
        usize i = 0;
        while (i <= len) {
            hash ^= vals[i++];
            hash *= prime;
        }
        
        return hash;
    }
    
    u32 FNV1A_32(const i8 vals[], usize len) {
        return FNV1A_32((const u8*)vals, len * 2);
    }

    
    //NOTE: this is only a valid way of doing things internal to the system,
    //since the output is dependent on endianness.
    u32 FNV1A_32(const u16 vals[], usize len) {
        return FNV1A_32((const u8*)vals, len * 2);
    }
    
    //NOTE: this is only a valid way of doing things internal to the system,
    //since the output is dependent on endianness.
    u32 FNV1A_32(const u32 vals[], usize len) {
        return FNV1A_32((const u8*)vals, len * 4);
    }
    
}
}
