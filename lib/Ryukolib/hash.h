/*
 * Miscellaneous Hash Functions
 * 
 * Copyright (C) 2018 ryukoposting <epg@tfwno.gf>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This file is part of the RyukOS operating system.
 */

#ifndef RYUKOLIB_HASH_H_
#define RYUKOLIB_HASH_H_

#include <ryukos.h>    // necessary so we can pull in the platform definition of u32

namespace rlib {
namespace hash {
    /* The Jenkins one-at-a-time hash function. Brutally simple, pretty
     * effective, not overwhelmingly performant. */
    u32 JenkinsOneAtATime(u8 *vals, usize len);
    
    //NOTE: this is only a valid way of doing things internal to the system,
    //since the output is dependent on endianness.
    u32 JenkinsOneAtATime(u16 *vals, usize len);
    
    //NOTE: this is only a valid way of doing things internal to the system,
    //since the output is dependent on endianness.
    u32 JenkinsOneAtATime(u32 *vals, usize len);
    
    /* FNV-1A, 32-bit flavor. Even simpler than Jenkins, and slightly
     * faster, too. Apparently it also strikes a great balance of
     * rare collisions and even distribution. */
    u32 FNV1A_32(const u8 vals[], usize len);
    u32 FNV1A_32(const i8 vals[], usize len);
    
    //NOTE: this is only a valid way of doing things internal to the system,
    //since the output is dependent on endianness.
    u32 FNV1A_32(const u16 vals[], usize len);
    
    //NOTE: this is only a valid way of doing things internal to the system,
    //since the output is dependent on endianness.
    u32 FNV1A_32(const u32 vals[], usize len);
    
}
}

#endif //RYUKOLIB_CRC32_H_

