/*
 * CRC-32 Engine Template
 * 
 * Copyright (C) 2018 ryukoposting <epg@tfwno.gf>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This file is part of the RyukOS operating system.
 */

#ifndef RYUKOLIB_CRC32_H_
#define RYUKOLIB_CRC32_H_

#include <ryukos.h>    // necessary so we can pull in the platform definition of u32

#include "noheap.h"

namespace rlib {
    
template<u32 polynomial, u32 initial_crc>
class CRC32 : NoHeap {
public:
    constexpr CRC32() ;
    
    u32 Generate(u8 array[]) ;
    
private:
    u32 crc_table[256];
    
};

}

template<u32 polynomial, u32 initial_crc>
constexpr rlib::CRC32<polynomial, initial_crc>::CRC32()  {
    u32 remainder = 0;
    u8 b = 0;
    do {
        remainder = b;
        for (unsigned int bit = 8; bit > 0; --bit) {
            if (remainder & 1)
                remainder = (remainder >> 1) ^ polynomial;
            else
                remainder = (remainder >> 1);
        }
        crc_table[b] = remainder;
    } while (++b);
}

template<u32 polynomial, u32 initial_crc>
u32 rlib::CRC32<polynomial, initial_crc>::Generate(u8 array[])  {
    u32 crc = ~initial_crc;
    for (const u8 &byte : array) {
        crc = (crc >> 8) ^ crc_table[(crc & 0xFF) ^ byte];
    }
    return ~crc;
}

#endif //RYUKOLIB_CRC32_H_
