/*
 * Miscellaneous helper macros
 * 
 * Copyright (C) 2018 ryukoposting <epg@tfwno.gf>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This file is part of the RyukOS operating system.
 */

#ifndef RYUKOLIB_MISC_H_
#define RYUKOLIB_MISC_H_

#include <ryukos.h>    //for u8 and the like

/**
 * @brief Returns the bit-reverse of a char, short, int, or long.
 */
#define bitreverse(_in_val) ({\
    auto out = _in_val;\
    switch (sizeof(out)) {\
        case 8:\
            out = ((out & 0xFFFFFFFF00000000UL) >> 32) | ((out & 0x00000000FFFFFFFFUL) << 32);\
        case 4:\
            out = ((out & 0xFFFF0000FFFF0000UL) >> 16) | ((out & 0x0000FFFF0000FFFFUL) << 16);\
        case 2:\
            out = ((out & 0xFF00FF00FF00FF00UL) >> 8)  | ((out & 0x00FF00FF00FF00FFUL) << 8);\
        case 1:\
            out = ((out & 0xF0F0F0F0F0F0F0F0UL) >> 4)  | ((out & 0x0F0F0F0F0F0F0F0FUL) << 4);\
            out = ((out & 0xCCCCCCCCCCCCCCCCUL) >> 2)  | ((out & 0x3333333333333333UL) << 2);\
            out = ((out & 0xAAAAAAAAAAAAAAAAUL) >> 1)  | ((out & 0x5555555555555555UL) << 1);\
            break;\
    }\
    out;\
})\

/**
 * @brief Reverse the endianess of an integral value.
 */
#define bytereverse(_in_val) ({\
    auto out = _in_val;\
    switch (sizeof(out)) {\
        case 8:\
            out = ((out & 0xFFFFFFFF00000000UL) >> 32) | ((out & 0x00000000FFFFFFFFUL) << 32);\
        case 4:\
            out = ((out & 0xFFFF0000FFFF0000UL) >> 16) | ((out & 0x0000FFFF0000FFFFUL) << 16);\
        case 2:\
            out = ((out & 0xFF00FF00FF00FF00UL) >> 8)  | ((out & 0x00FF00FF00FF00FFUL) << 8);\
        case 1:\
            break;\
    }\
    out;\
})\

#define rlib_checktype(var, type) {\
    __PRAGMA(GCC diagnostic push);\
    __PRAGMA(GCC diagnostic error "-Wincompatible-pointer-types");\
    __typeof(var) *__t;\
    __t = (type*)NULL;\
    __PRAGMA(GCC diagnostic pop);\
}
//rlib_checktype(some_variable,some_scalar_type)

//source: http://www.pixelbeat.org/programming/gcc/static_assert.html
#define rlib_checksize_(e) extern char (*ct_assert(void)) [sizeof(char[1 - 2*!(e)])]
#define rlib_checksize(item,size) rlib_checksize_((sizeof(item)==size))
//rlib_checksize(int,4);

namespace rlib {
    
    template<usize size>
    union ByteArray {
        u8 bytes[size];
        u16 words[size/2];
        u32 dwords[size/4];
        
        u8 &operator[] (usize);
    };
    
    template<usize size>
    u8 &ByteArray<size>::operator[] (usize index) {
        return bytes[index];
    }
}

#endif //RYUKOLIB_MISC_H_
