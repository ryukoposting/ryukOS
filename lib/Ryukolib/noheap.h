/*
 * Ryukolib No-Heap Attribute Class
 * 
 * Copyright (C) 2018 ryukoposting <epg@tfwno.gf>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This file is part of the RyukOS operating system.
 */

#ifndef RYUKOLIB_NOHEAP_H_
#define RYUKOLIB_NOHEAP_H_

#include <ryukos.h>    // necessary so we can pull in the platform definition of usize

namespace rlib {

class NoHeap {
protected:
    // don't allow heap allocation
    void *operator new(usize);
    void *operator new[](usize);
};

}

#endif          // HELPER_NOHEAP_H_
