/*
 * Ryukolib FIFO Option Structure
 * 
 * Copyright (C) 2018 ryukoposting <epg@tfwno.gf>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This file is part of the RyukOS operating system.
 */

#ifndef RYUKOLIB_OPTION_H_
#define RYUKOLIB_OPTION_H_

namespace rlib {
    
    template<typename T>
    struct Option {
    private:
        enum { SOME, NONE } result;
        
        T item;

    public:
        Option(T& value) {
            item = value;
            result = SOME;
        }
        
        Option() {
            result = NONE;
        }
        
        bool IsSome() const { return result == SOME; }
        
        bool IsNone() const { return result == NONE; }
        
        T& Unwrap() { return item; }

        // allows for if ((auto it = option_decl)) {option_decl.item}
        operator bool()         { return result == SOME; }
        operator int()          { return result == SOME; }
        operator unsigned int() { return result == SOME; }
        
        bool operator !()       { return result != SOME; }
    };
    
}


#endif      // RYUKOLIB_OPTION_H_
