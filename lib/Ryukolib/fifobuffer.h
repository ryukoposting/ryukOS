/*
 * Ryukolib FIFO Circular Buffer
 * 
 * Copyright (C) 2018 ryukoposting <epg@tfwno.gf>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This file is part of the RyukOS operating system.
 */

#ifndef RYUKOLIB_FIFOBUFFER_H_
#define RYUKOLIB_FIFOBUFFER_H_

#include <ryukos.h>    // necessary so we can pull in the platform definition of usize

#include "noheap.h"
#include "option.h"

namespace rlib {

template<typename T, usize max_len>
class FifoBuffer {
public:
    FifoBuffer() {
        for (auto &it : array) {
            it = 0;
        }
    }
    
    bool IsEmpty() const  { return length == 0; }
    
    bool IsFull() const  { return length == max_len; }
    
    usize Length() const  { return length; }
    
    const usize max_length = max_len;
    
    bool Push(T element)  {
        if (IsFull()) return false;
        array[((first + length) % max_len)] = element;
        ++length;
        return true;
    }
    
    bool PushFront(T &element)  {
        return Insert(0, element);
    }
    
    rlib::Option<T> Pop()  {
        if (IsEmpty()) return rlib::Option<T>();

        --length;
        return rlib::Option<T>(array[first++]);
    }
    
    rlib::Option<T> PopRear()  {
        return Remove(length - 1);
    }
    
    T& operator[](usize index)  {
        return array[((first + index) % max_len)];
    }
    
    rlib::Option<T> Peek(usize index) const  {
        if (IsEmpty() || (index >= length)) return rlib::Option<T>();

        return rlib::Option<T>::Some(array[((first + index) % max_len)]);
    }
    
    rlib::Option<T> Remove(usize index)  {
        if (IsEmpty() || (index >= length)) return rlib::Option<T>();

        auto out = (*this)[index];
        
        if (index < (length >> 1)) {
            for (auto i = index; i > 0; --i) {
                (*this)[i] = (*this)[i - 1];
            }
            ++first;
        } else {
            for (auto i = index; i < (length - 1); ++i) {
                (*this)[i] = (*this)[i + 1];
            }
        }
        --length;
        return out;
    }
    
    bool Insert(usize index, T element)  {
        if (IsFull()) return false;
        
        for (auto i = length++; i > index; --i) {
            (*this)[i] = (*this)[i - 1];
        }
        
        (*this)[index] = element;
        return true;
    }
    

private:
    T array[max_len];
    
    usize first = 0;
    usize length = 0;
    
};

}

#endif //RYUKOLIB_FIFOBUFFER_H_
