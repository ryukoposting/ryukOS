// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#ifndef _POSIX_SEMAPHORE_H_
#define _POSIX_SEMAPHORE_H_

#include <ryukos.h>

#define SEM_VALUE_MAX 0x100

#define SEM_STATICS_AVAILABLE 32

#ifdef __cplusplus
extern "C" {
#endif
    
    typedef union _semaphore {
        union _semaphore *__attribute__((aligned(4))) next;
        struct {
            i16 tid;
            i8 val;
            i8 max;
            int _mno;
        };
    } __attribute__((packed)) _sem_t;
    
    typedef _sem_t* sem_t;
    
    int sem_init(sem_t *sem, int pshared, i8 value);

    int sem_wait(sem_t *sem);

    //     int sem_timedwait(sem_t *sem,);

    int sem_post(sem_t *sem);

    int sem_destroy(sem_t *sem);

    int sem_getvalue(sem_t *restrict sem, int *restrict sval);

#ifdef __cplusplus
}
#endif

#endif //_POSIX_SEMAPHORE_H_
