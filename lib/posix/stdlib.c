#include <ryukos.h>
#include "stdlib.h"
#include "stdbool.h"

bool _is_in(char v, const char *st, usize n)
{
    for (usize i = 0; i < n; ++i) {
        if (v == st[i]) {
            return true;

        }
    }

    return false;
}

int abs(int n)
{
    return (n < 0) ? -n : n;
}

long labs(long n)
{
    return (n < 0) ? -n : n;
}

long long llabs(long long n)
{
    return (n < 0) ? -n : n;
}

int atoi(const char *str)
{
    register const char *s = str;

    bool is_negative = false;
    int out;

    for (; _is_in(*s, " \t-", sizeof(" \t-") - 1); ++s) {
        if (*s == '-') {
            is_negative = true;
            ++s;
            break;
        }
    }

    if ((*s == '0') && (!is_negative)) {
        switch (*(++s)) {
        case 'x':
            ++s;

            for (out = 0; (((*s) >= '0') && ((*s) <= '9')) || (((*s) >= 'A') && ((*s) <= 'F')); ++s) {
                out *= 16;

                if (((*s) >= 'A') && ((*s) <= 'F')) out += ((*s) - 55);

                else                                out += ((*s) - '0');
            }

            return out;

        case 'b':
            ++s;

            for (out = 0; (((*s) == '0') || ((*s) == '1')); ++s) {
                out <<= 1;
                out += ((*s) == '1');
            }

            return out;

        case 'o':
            ++s;

            for (out = 0; (((*s) >= '0') && ((*s) <= '7')); ++s) {
                out *= 8;
                out += ((*s) - '0');
            }

            return out;

        default:
            if (((*s) < '0') || ((*s) > '9')) return 0;
        }
    }

    for (out = 0; ((*s) >= '0') && ((*s) <= '9'); ++s) {
        out *= 10;
        out += ((*s) - 48);
    }

    return (is_negative) ? (0 - out) : out;
}


void abort(void)
{
    //TODO: this should free the task's allocated memory space and
    //      remove it from the scheduler.
}
