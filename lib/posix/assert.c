#include "assert.h"
#include <string.h>
#include <sys/interrupts/interrupts.h>


char __attribute__((section(".assert"))) _assert_message[128];

void _onassertfail(char const *const message, char const *const file, char const *const line)
{
    interrupts_disable();
    strncpy(_assert_message, message, 80);
    strncat(_assert_message, file, 127);
    strncat(_assert_message, line, 127);

    while (1);
    asm volatile("nop");
}

