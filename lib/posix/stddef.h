
#ifndef _POSIX_STDDEF_H_
#define _POSIX_STDDEF_H_

#include <ryukos.h>

//32-bit platforms
#if defined(RYUKOS_PLATFORM_ARM_M4) || defined(RYUKOS_PLATFORM_ARM_M3)

#ifndef NULL
#define NULL 0
#endif  //NULL

#endif  //RYUKOS_PLATFORM_ARM || RYUKOS_PLATFORM_ARM_M3

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif //_POSIX_STDDEF_H_

