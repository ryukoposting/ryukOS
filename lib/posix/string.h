
#ifndef _POSIX_STRING_H_
#define _POSIX_STRING_H_

#include <ryukos.h>

#ifdef __cplusplus
extern "C" {
#endif

    int memcmp(const void *p1, const void *p2, usize n);

    void *memcpy(void *restrict dest, const void *restrict src, usize n);
    
    void *memset(void * dest, int c, usize n);

    char *strcpy(char *restrict dest, const char *restrict src);

    char *strncpy(char *restrict dest, const char *restrict src, usize n);

    usize strlen(const char *str);

    usize strnlen(const char *str, usize n);

    char *strcat(char *restrict dest, const char *restrict app);

    char *strncat(char *restrict dest, const char *restrict app, usize n);

#ifdef __cplusplus
}
#endif


#endif  //_POSIX_STRING_H_
