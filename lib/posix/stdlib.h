
#ifndef _POSIX_STDLIB_H_
#define _POSIX_STDLIB_H_

#include <ryukos.h>

#ifndef NULL
#define NULL 0
#endif

#ifdef __cplusplus
extern "C" {
#endif

    int abs(int n);

    long labs(long n);

    long long llabs(long long n);

    f64 atof(const char *);

    int atoi(const char *);

    void free(void *);
    
    void *malloc(usize nbytes);

    void abort(void);

#ifdef __cplusplus
}
#endif


#endif  //_POSIX_STDLIB_H_
