#include <ryukos.h>
#include "string.h"

int memcmp(const void *p1, const void *p2, usize n)
{
    register const u8 *r1 = (const u8 *)p1;
    register const u8 *r2 = (const u8 *)p2;

    while ((n--) && (*r1 == *r2)) {
        ++r1;
        ++r2;
    }

    if (n == 0) return 0;

    else return (*r1 < *r2) ? -1 : 1;
}

void *memcpy(void *__restrict dest, const void *__restrict src, usize n)
{
    register u8 *r1 = (u8 *)dest;
    register const u8 *r2 = (const u8 *)src;

    while (n--) {
        *r1 = *r2;
        ++r1;
        ++r2;
    }

    return dest;
}

void *memset(void * dest, int c, usize n)
{
    register u8 *r1 = (u8 *)dest;

    while (n--) {
        *r1 = (u8)c;
        ++r1;
    }

    return dest;
}

char *strcpy(char *__restrict dest, const char *__restrict src)
{
    register char *dst = dest;

    while ((*dst = *src)) {
        ++dst;
        ++src;
    }

    return dest;
}

char *strncpy(char *__restrict dest, const char *__restrict src, usize n)
{
    register char *dst = dest;

    while ((*dst = *src) && (n--)) {
        ++dst;
        ++src;
    }

    return dest;
}

usize strlen(const char *str)
{
    register usize out = 0;

    while ((*(str++))) ++out;

    return out;
}

usize strnlen(const char *str, usize n)
{
    register usize out = 0;

    while ((*(str++)) && (n--)) ++out;

    return out;
}

char *strcat(char *__restrict dest, const char *__restrict app)
{
    register char *dst = dest;

    while (*(dst++));

    --dst;
    --dst;

    while ((*(dst++) = *(app++)));

    return dest;
}

char *strncat(char *__restrict dest, const char *__restrict app, usize n)
{
    register char *dst = dest;

    while (*(dst++));

    --dst;

    while (((*(dst++) = *(app++))) && (n--));

    *dst = 0;

    return dest;
}
