// Copyright 2018 ryukoposting
// 
// Licensed under the Apache License, Version 2.0 (the "License")
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// This file is part of the RyukOS operating system.

#include "semaphore.h"
#include "errno.h"
#include <sys/thread/thread.h>
#include <sys/sbrk/sbrk.h>
#include <sys/kmutex/kmutex.h>

_sem_t sems[SEM_STATICS_AVAILABLE];
static _sem_t *head;

static int deletesem(sem_t *sem)
{
    if ((*sem)->val >= 0) {
        (*sem)->_mno = 0;
        (*sem)->val = -1;
        (*sem)->tid = -1;
        (*sem)->next = head;
        head = *sem;
        return 0;
        
    } else {
        return -1;
    }
}

static int genmno(sem_t *sem)
{
    return ((*sem)->max * (*sem)->tid) ^ 0xDEADBEEF;
}

// 0 if sem's magic number is invalid
static int checkmno(sem_t *sem)
{
    return (*sem)->_mno == (((*sem)->max * (*sem)->tid) ^ 0xDEADBEEF);
}

// 0 if sem doesn't exist or is invalid
static int semcheck(sem_t *sem)
{
    if (!isalive((*sem)->tid)) {
        if (checkmno(sem)) deletesem(sem);
        return 0;
        
    } else return checkmno(sem);
}

void sem_sysinit()
{
    for (u8 i = 0; i < (SEM_STATICS_AVAILABLE - 1); ++i) {
        sems[i].next = &sems[i + 1];
    }
    
    sems[SEM_STATICS_AVAILABLE - 1].next = NULL;
    head = sems;
}

//TODO: implement pshared to make scheduling smarter
int sem_init(sem_t *sem, int pshared, i8 value)
{
    UNUSED(pshared);
    int out = -2;
    
    KMUTEX_BEGIN();
    
    if (head != NULL) {
        (*sem) = head;
        head = head->next;
        out = 0;
        
    } else if (((*sem) = kmalloc(sizeof(_sem_t) * 4)) == NULL) {
        errno = ENOMEM;
        out = -1;
            
    } else {
        head = sem[1];
        head[0].next = &head[1];
        head[1].next = &head[2];
        head[2].next = NULL;
        out = 0;
    }
    
    assert(out != -2, "out not set in semaphore init");
    
    if (out == 0) {
        (*sem)->tid = threadid();
        (*sem)->val = value;
        (*sem)->max = value;
        (*sem)->_mno = genmno(sem);
    }
    
    KMUTEX_END();
    
    return out;
}

// BUG: edge case where preemption occurs immediately after breakout from
// while loop
int sem_wait(sem_t *sem)
{
    while (((*sem)->val == 0) && semcheck(sem)) {
        yield((*sem)->tid);
    }

    if (((*sem)->val <= (*sem)->max) && ((*sem)->val > 0) && semcheck(sem)) {
        --((*sem)->val);
        return 0;
        
    } else {
        errno = EEXIST;
        return -1;
    }
}

int sem_post(sem_t *sem)
{
    if ((*sem)->val < (*sem)->max) {
        ++((*sem)->val);
        return 0;
        
    } else if (semcheck(sem) == 0) {
        errno = EEXIST;
        return -1;
        
    } else {
        errno = E2BIG;
        return -1;
    }
}

int sem_destroy(sem_t *sem)
{
    int out = -2;
    
    KMUTEX_BEGIN();
    
    out = semcheck(sem) ? deletesem(sem) : -1;
    
    KMUTEX_END();
    
    assert(out != -2, "out not set in sem_destroy");
    
    return out;
}

int sem_getvalue(sem_t *restrict sem, int *restrict sval)
{
    if (semcheck(sem)) {
        *sval = (*sem)->val;
        return 0;
        
    } else {
        errno = EEXIST;
        return -1;
    }
}
