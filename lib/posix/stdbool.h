
#ifndef _POSIX_STDBOOL_H_
#define _POSIX_STDBOOL_H_

#include <ryukos.h>

#ifndef __cplusplus

#ifdef u8
#define _Bool u8
#else
#define _Bool unsigned char
#endif

#define bool _Bool

#define true 1
#define false 0

#endif


#endif  //_POSIX_STDBOOL_H_
