
#ifndef _POSIX_ASSERT_H_
#define _POSIX_ASSERT_H_

#include <ryukos.h>

#ifdef __cplusplus
extern "C" {
#endif

    #ifndef NDEBUG
    extern void _onassertfail(char const *const message, char const *const file, char const *const line);
    #endif
    extern char __attribute__((section(".assert"))) _assert_message[128];

    #ifdef NDEBUG
    #define assert(ignore,msg)((void) 0)
    #else
    #define assert(_test,_msg) {\
            if (!(_test)) {\
                _onassertfail(_msg,STRINGIFY(__FILE__),STRINGIFY(__LINE__));\
            }\
        }
    #endif


    #ifdef NDEBUG_CT
    #define ctassert(ignore,msg)((void) 0)
    #elif (__STDC_VERSION__ == 201112L)
    #define ctassert(e,msg) _Static_assert(e,msg)
    #elif defined(__cpp_static_assert)
    #define ctassert(e,msg) static_assert(e,msg)
    #else
    /**
    * @brief Compile-time assertion macro for use in C/C++ implementation files.
    *
    * source: http://www.pixelbeat.org/programming/gcc/static_assert.html
    *
    * This function does not consistently work in headers. It should always be
    * used in implementation files.
    */
    #define ctassert(e,msg) extern char (*MACRO_CONCAT(ct_assert,__LINE__)(void)) [sizeof(char[1 - 2*!(e)])]
    #endif

#ifdef __cplusplus
}
#endif


#endif  //_POSIX_ASSERT_H_

