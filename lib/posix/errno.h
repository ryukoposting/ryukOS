#ifndef _POSIX_ERRNO_H_
#define _POSIX_ERRNO_H_

#include <ryukos.h>

#ifdef __cplusplus
extern "C" {
#endif

    #define ___DUP_OF(macro_name) macro_name

    #define ierror int

    __attribute__((section(".errno")))
    extern volatile ierror errno;

    #define E2BIG           1
    #define EACCES          2
    #define EADDRINUSE      3
    #define EADDRNOTAVAIL   4
    #define EAFNOSUPPORT    5
    #define EAGAIN          6
    #define EALREADY        7
    #define EBADF           8
    #define EBADMSG         9
    #define EBUSY           10
    #define ECANCELED       11
    #define ECHILD          12
    #define ECONNABORTED    13
    #define ECONNRESET      14
    #define EDEADLK         15
    #define EDESTADDRREQ    16
    #define EDOM            17
    #define EDQUOT          18
    #define EEXIST          19
    #define EFAULT          20
    #define EFBIG           21
    #define EHOSTUNREACH    22
    #define EIDRM           23
    #define EILSEQ          24
    #define EINPROGRESS     25
    #define EINTR           26
    #define EINVAL          27
    #define EIO             28
    #define EISCONN         29
    #define EISDIR          30
    #define ELOOP           31
    #define EMFILE          32
    #define EMLINK          33
    #define EMSGSIZE        34
    #define EMULTIHOP       35
    #define ENAMETOOLONG    36
    #define ENETDOWN        37
    #define ENETRESET       38
    #define ENETUNREACH     39
    #define ENFILE          40
    #define ENOBUFS         41
    #define ENODATA         42
    #define ENODEV          43
    #define ENOENT          44
    #define ENOEXEC         45
    #define ENOLCK          46
    #define ENOLINK         47
    #define ENOMEM          48
    #define ENOMSG          49
    #define ENOPROTOOPT     50
    #define ENOSPC          51
    #define ENOSR           52
    #define ENOSTR          53
    #define ENOSYS          54
    #define ENOTCONN        55
    #define ENOTDIR         56
    #define ENOTEMPTY       57
    #define ENOTRECOVERABLE 58
    #define ENOTSOCK        59
    #define ENOTSUP         60
    #define EOPNOTSUPP      ___DUP_OF(ENOTSUP)
    #define ENOTTY          61
    #define ENXIO           62
    #define EOVERFLOW       63
    #define EOWNERDEAD      64
    #define EPERM           65
    #define EPIPE           66
    #define EPROTO          67
    #define EPROTONOSUPPORT 68
    #define EPROTOTYPE      69
    #define ETIME           70
    #define ETIMEDOUT       71
    #define EWOULDBLOCK     72

#ifdef __cplusplus
}
#endif

#endif  //_POSIX_ERRNO_H_

