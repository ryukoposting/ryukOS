This project is still deep in development.

![RyukOS](https://i.imgur.com/ok4CuHG.png)

*This image is based on the work of DeviantArt user [plotnishek](https://www.deviantart.com/plotnishek)*

[The RyukOS homepage](http://dailyprog.org/~ryukoposting/ryukos.html)

[RyukOS on Flatlanders](https://flatlanders.net/h#/group/8/details/stash/47/details)

## Summary

- RyukOS is an operating system using an extremely compact kernel. By
  default, RyukOS uses preemptive multitasking, but cooperative multitasking
  is also supported. Using the preemptive mode, RyukOS is a hard real-time
  operating system. It uses EDF scheduling.
- RyukOS is designed for single-core microcontrollers with on-board flash.
  It is specifically meant for use in embedded systems.
- RyukOS is being developed primarily for the sake of research and
  experimentation. It uses its own stripped-down libc, which interfaces
  with the kernel more closely than an independent libc otherwise could.
  This is, in part, what enables the RyukOS scheduler to optimize CPU
  usage and guarantee service to real-time threads.
- RyukOS is being tested on an ARM Cortex-M4 microcontroller (the Infineon
  XMC4700-2048). It is being made primarily with Cortex-M and RISC-V in mind.

## Building and Using RyukOS

To build a generic ARM Cortex-M4 version of the RyukOS kernel, compatible with
any standards-compliant CM4 processor:

1. Clone the repository, and cd into the ./build directory.
2. Open the Makefile.
3. There are 2 variables declared at the top of the makefile: TARGET_UC_REPO,
   and TARGET_UC. Change these to `TARGET_UC_REPO=ryukos-cm4-generic-deps`
   and `TARGET_UC=cm4-generic`
4. Now, just run the makefile by running the plain old `make` command.

To build RyukOS for the Infineon XMC4700-F144x2048:

1. Clone the repository, and cd into the ./build directory.
2. Open the Makefile.
3. There are 2 variables declared at the top of the makefile: TARGET_UC_REPO,
   and TARGET_UC. Change these to `TARGET_UC_REPO=ryukos-xmc4700-deps`
   and `TARGET_UC=xmc4700-F144x2048`
4. Now, just run the makefile by running the plain old `make` command.

The builds above will both create a .bin, which is flashed to the target
microcontroller, a .elf file which is the raw binary (it can be loaded into
debugger tools like SEGGER Ozone), a .map file showing a memory map of the
binary outputs, and a .S file, which is the complete disassembly of the
output binary.

Note: if you are getting an error that reads like "address ..... of ryukos.elf
section .heap is not within region...", then navigate to ./ryukos.h, and
reduce the RYUKOS_MAX_HEAP macro until it works. If compiling with the generic
Cortex-M4 build, 0x2000 is a workable value.

## KDevelop Files

I accidentally pushed the project files [KDevelop](https://www.kdevelop.org/)
generated to the repository, but I have decided to leave them because it
provides an easy way for me to sync the project across multiple computers.
Just ignore them if you don't use KDevelop.

## License

Licensed under the Apache License, Version 2.0 (the "License")
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
